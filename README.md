Course project for CS 122B at UC Irvine in Winter 2015.

* Collaborated in a team of four to create an e-commerce web application to simulate a movie rental service
* Used Apache Tomcat, JDBC, JavaScript, AJAX, HTML, CSS, and MySQL to build the application locally
* Implemented user registration, search functionality, results with pagination, and a shopping cart feature