<%@ page import="java.util.*" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>FabFlix</title>

<!-- GET BOOTSTRAP CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<style>
		body { padding-top: 70px; }
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FabFlix</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/home.jsp">HOME</a></li>
        <li><a href="${pageContext.request.contextPath}/advancedsearch.jsp">ADVANCED SEARCH</a></li>
        <li><a href="${pageContext.request.contextPath}/cart.jsp">CART</a></li>
		<li class="active"><a href="${pageContext.request.contextPath}/servlet/checkout">CHECKOUT<span class="sr-only">(current)</span></a></li>        
        </ul>
        <h5><div align="right">Welcome ${username}! <a href="${pageContext.request.contextPath}/servlet/logout"><label for="ex1">Logout</label></a></div></h5>
        </div>
        </div>
    </nav>

	<div class="container">
	<label style="text-center">Customer Information</label>
		<div class="container">
	  		<form role="form" method="post" action="${pageContext.request.contextPath}/servlet/checkout">
	  	  		<div class="input-group">
	 		    <span class="input-group-addon" id="basic-addon1">Card Number:&nbsp;&nbsp;&nbsp;</span>
	   	    		<input type="text" class="form-control" name="card_num">
	   	  		</div>
	   	  		<br/>
	      		<div class="input-group">
	 		    <span class="input-group-addon" id="basic-addon1">Expiration Date:</span>
	      			<input type="text" class="form-control" name="exp_date">
				</div>
				<br/>
				<div class="input-group">
	 		    <span class="input-group-addon" id="basic-addon1">First Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	      			<input type=text class="form-control" name="first_name">
				</div>
				<br/>
				<div class="input-group">
				 	<span class="input-group-addon" id="basic-addon1">Last Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	      			<input type="text" class="form-control" name="last_name">
				</div>
				<br/>
				
				<center>
					<div class="form-group">
	        			<button type="submit" class="btn btn-default">Checkout</button>
	      			</div>
				</center>
			</form>
		</div>
	</div>




</body>
</html>