<%@ page import="com.FabFlix5.*, Actor.*, Movie.*, java.util.*" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<% if (session.getAttribute("loginSuccess") == null || !((Boolean)session.getAttribute("loginSuccess"))) { %>
		<% System.out.println("Not logged in"); //This is not working right now %>
		<jsp:forward page="/index.jsp" />
	<% } %>

	<% 
		double total = 0;
		Cart cart = new Cart();
		if (session.getAttribute("cart") != null) 
			cart = (Cart) session.getAttribute("cart");
	%>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>FabFlix</title>
	<style>
		body { padding-top: 70px; }
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FabFlix</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/home.jsp">HOME</a></li>
        <li><a href="${pageContext.request.contextPath}/advancedsearch.jsp">ADVANCED SEARCH</a></li>
        <li class="active"><a href="${pageContext.request.contextPath}/cart.jsp">CART<span class="sr-only">(current)</span></a></li>
        <li><a href="${pageContext.request.contextPath}/servlet/checkout">CHECKOUT</a></li>      
        </ul>
        <h5><div align="right">Welcome ${username}! <a href="${pageContext.request.contextPath}/servlet/logout"><label for="ex1">Logout</label></a></div></h5>
        </div>
        </div>
    </nav>
        
	<div class="container">
	  	<h2>Shopping cart</h2>
	    <div class="table-responsive">          
			<table class="table">
				<thead>
		          	<tr>
		           		<th>Title</th>
		            	<th>Price</th>
		            	<th>Quantity</th>
		            	<th></th>
		            	<th></th>
		          	</tr>
        		</thead>
				<tbody>
					<%
					for (Map.Entry<Item, Integer> entry : cart.cart.entrySet())
					{ 
						total += (15.99 * entry.getValue());
						%>
						<tr>
	            			<td>
	            				<h5><%= entry.getKey().movieTitle %></h5>
	            			</td>
	            			<td>
	            				<h5>$<%= (15.99* entry.getValue()) %></h5>
	            			</td>
	            			<td>
								<form role="form" action="${pageContext.request.contextPath}/servlet/cart?action=update&mid=<%=entry.getKey().movieId %>"" method="post">
      								<div class="col-xs-4"><input class="form-control" name="quantity" value="<%=entry.getValue() %>"></div>
							</td>
							<td>
	            					<a href="${pageContext.request.contextPath}/servlet/cart?action=update&mid=<%=entry.getKey().movieId %>"><button type="submit" class="btn btn-default">Update</button></a>
	            				</form>
	            			</td>
	            			<td>
	            			<a href="${pageContext.request.contextPath}/servlet/cart?action=remove&mid=<%=entry.getKey().movieId %>"><button type="submit" class="btn btn-default">Remove</button></a>
	            			</td>
	            		</tr>
					<%
					}	
					%>
		   	</table>
		   	<a href="${pageContext.request.contextPath}/servlet/cart?action=clear&mid=0"><button type="submit" class="btn btn-default">Clear cart</button></a>
		    <a href="${pageContext.request.contextPath}/checkout.jsp"><button type="submit" class="btn btn-default">Checkout</button><div align="right"></a><h5>Total : $<%=total %></h5></div>
		</div>
	</div>
</body>
</html>