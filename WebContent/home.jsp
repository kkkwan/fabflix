<%@ page import="java.util.*" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<% if (session.getAttribute("loginSuccess") == null || !((Boolean)session.getAttribute("loginSuccess"))) { %>
		<% System.out.println("Not logged in"); //This is not working right now %>
		<jsp:forward page="/index.jsp" />
	<% } %>
	
	<% List<String> genres = (List<String>) session.getAttribute("genreList"); %>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>FabFlix</title>
	
	<script type="text/javascript">
		window.onload = function() {
        	var a = document.getElementById("genreId");

        	a.onclick = function() {
				console.log("clicked");
        		return false;
        	}
        }
		
		function selectedItem(sel)
		{
			var val = sel.value;
			if (sel)
			{
				sel.outerHTML = "";
				delete sel;
			}
			
			document.getElementById("movieName").value = val
			document.getElementById("movieName").focus()
		}

		function loadXMLDoc()
		{
			var xmlhttp;
			var movieName = document.getElementById("movieName")

			if (movieName.value.length == 0)
			{
				var listBox = document.getElementById("listBox")
				
				if (listBox)
				{
					listBox.outerHTML = "";
					delete listBox;
				}
			} else {
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
						document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
				}
				xmlhttp.open("GET","${pageContext.request.contextPath}/servlet/ajax?movieName=" + movieName.value, true);
				xmlhttp.send();
			}
		}
	</script>
	<style>
		body { padding-top: 70px; }
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FabFlix</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="${pageContext.request.contextPath}/home.jsp">HOME<span class="sr-only">(current)</span></a></li>
        <li><a href="${pageContext.request.contextPath}/advancedsearch.jsp">ADVANCED SEARCH</a></li>
        <li><a href="${pageContext.request.contextPath}/cart.jsp">CART</a></li>
        <li><a href="${pageContext.request.contextPath}/servlet/checkout">CHECKOUT</a></li>
        </ul>
        <h5><div align="right">Welcome ${username}! <a href="${pageContext.request.contextPath}/servlet/logout"><label for="ex1">Logout</label></a></div></h5>
        </div>
        </div>
        </nav>
	
	<br>
	<div class="container">
		  	<form role="form" action="${pageContext.request.contextPath}/servlet/search" method="post">
			<div class="form-group">
    			<div class="col-md-5">
        			<label for="ex1">Search for a movie</label>
        			<input class="form-control" id="movieName" name="movieName" type="text" onkeyup="loadXMLDoc()">
        			<div id="myDiv"></div>
      			</div>
			</div>
		</form>
	</div>
	<br>
	
	<%
	String[] chars= {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
	%>
	<div class="container">
   		<div class="col-md-5">
        	<label for="ex1">Search for a title</label>
        	<br>
        	<% for (int i = 0; i < 10; i++) { %>
        		<a id="titleName" href="${pageContext.request.contextPath}/servlet/search?titleName=<%=i %>"> <%= i %> </a>
        	<% } %>
        	<% for (String charz : chars) { %>
        		<a id="titleName" href="${pageContext.request.contextPath}/servlet/search?titleName=<%=charz %>"> <%= charz %> </a>
        	<% } %>
        </div>
	</div>
	
	<br><br>
	
	<div class="container">
		<div class="col-md-5">
			<label for="ex1">Search for a genre</label>
			<br>
			<% int counter = 0;
			for (String genre : genres) { %>
				<a href="${pageContext.request.contextPath}/servlet/search?genre=<%=genre %>"> <%= genre %> </a>
				<% 
					counter++;
					if (counter == 5) { %>
						<br>
					<%	counter = 0;
					}	
				} %>
		</div>
	</div>
</body>
</html>