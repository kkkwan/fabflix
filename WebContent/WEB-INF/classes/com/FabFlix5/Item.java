package com.FabFlix5;

import java.util.List;

public class Item
{
	public String movieId;
	public String movieTitle;
	
	public Item()
	{
		movieId = "";
		movieTitle = "";
	}
	
	public Item(String movieId, String movieTitle)
	{
		this.movieId = movieId;
		this.movieTitle = movieTitle;
	}
}