package com.FabFlix5;

import java.util.List;

public class MovieListing
{

	public String title;
	public String year;
	public String director;
	public String movieId;
	public List<String> stars;
	public List<String> genres;
	public String price;
	public String bannerUrl;
	public String trailerUrl;
	
	public MovieListing(String title, String year, String director, String movieId, List<String> stars, List<String> genres, String bannerUrl, String trailerUrl)
	{
		this.title = title;
		this.year = year;
		this.director = director;
		this.movieId = movieId;
		this.stars = stars;
		this.genres = genres;
		this.price = "$15.99"; //Static price
		this.bannerUrl = bannerUrl;
		this.trailerUrl = trailerUrl;
	}

}
