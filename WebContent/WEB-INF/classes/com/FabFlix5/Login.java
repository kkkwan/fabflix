package com.FabFlix5;
/* A servlet to display the contents of the MySQL movieDB database */

import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class Login extends HttpServlet
{
	
	public boolean loginSuccess = false;
	
    public String getServletInfo()
    {
       return "Servlet connects to MySQL database and displays result of a SELECT";
    }

    // Use http GET
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
    }
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = (String) request.getParameter("username");
    	String password = (String) request.getParameter("password");
    	String url="";
    	HttpSession session = request.getSession();
    	List<String> genreList = null;

    	try
    	{
    		Connection dbcon = Database.SQLPoolBridge.getConnection();
    		
			Statement statement = dbcon.createStatement();
			String loginQuery = "select password, id FROM customers WHERE LOWER(email)=\"" + username.toLowerCase() + "\"";
			ResultSet loginResults = statement.executeQuery(loginQuery);
			
			if (loginResults.next())
			{
				if (password.equals(loginResults.getString("password")))
				{
					loginSuccess = true;
					session.setAttribute("cid", loginResults.getString("id"));
				}
			}
			
			genreList = new ArrayList<String>();
			System.out.println("2");
			
			String genreQuery = "select distinct g.name as genres "
					+ "from movies m, stars s, stars_in_movies sm, genres g, genres_in_movies gm "
					+ "where m.id = sm.movie_id and s.id = sm.star_id and g.id = gm.genre_id and m.id = gm.movie_id order by genres asc";
			ResultSet genreResults = statement.executeQuery(genreQuery);
			
			while (genreResults.next())
			{
				genreList.add(genreResults.getString("genres"));
			}
			System.out.println("3");	
    	} catch (SQLException ex) {
    		loginSuccess = false;
    	} catch (java.lang.Exception ex) {
    		loginSuccess = false;
    	}
	    
	    if (loginSuccess)
	    {
	    	url = "/home.jsp";
	    } else
	    {
	    	url = "/index.jsp";
	    }
	    
	    ServletContext sc = getServletContext();
	    RequestDispatcher rd = sc.getRequestDispatcher(url);
	    
		session.setAttribute("genreList", genreList);
        session.setAttribute("loginSuccess", loginSuccess);
        session.setAttribute("username", username);
        session.setAttribute("password", password);
		
		rd.forward(request, response);
	}
}