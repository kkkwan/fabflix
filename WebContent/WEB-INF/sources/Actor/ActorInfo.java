package Actor;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/*
 * 
 * This class is in desperate need of refactoring
 * 
 */

public class ActorInfo extends HttpServlet {

	/*
	 * CHANGE ME TO WHAT YOUR PROJECT/WAR FILE IS CALLED
	 */

	// need to find soluion for relative path
	private static final String serverRoot = "FabFlix5";

	public String getServletInfo() {
		return "Displays information related to a single actor and movies they starred in from the database";
	}

	// Use http GET
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html"); // Response mime type
		String name = request.getParameter("name");
		System.out.println("name: " + name);
		String first_name = name.split(" ")[0];
		String last_name = name.split(" ")[1];

		PrintWriter out = response.getWriter();
		if (name == null) {
			// go to some other page
			out.println("ActorInfo:doGet::EMPTY_QUERY");
			return;
		}

		try {
			// user SQLPoolBridge and get a Pooled Connection
			Connection dbcon = Database.SQLPoolBridge.getConnection();
			// Declare our statement
			Statement statement = dbcon.createStatement();

			// initial movie query
			ResultSet rs = statement
					.executeQuery("SELECT * FROM stars WHERE first_name=\"" + first_name
							+ "\"" + " and last_name = \"" + last_name + "\"");

			// default to a "move not found: error page if we get a bad query
			if (!rs.next()) {
				out.println("actorInfo:doGet::ACTOR_NOT_FOUND");
				return;
			}
			
			// grab results from query (all movies that an actor starred in)
			Actor actor = new Actor(rs.getString("id"), rs.getString("first_name"),
					rs.getString("last_name"), rs.getString("dob"),
					rs.getString("photo_url"));
			rs = statement
					.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url "
							+ "FROM movies as m, stars_in_movies as s2m "
							+ "WHERE m.id = s2m.movie_id AND s2m.star_id=\""
							+ actor.id + "\"");

			HttpSession session = request.getSession();
			session.setAttribute("actorObj", actor);
			
			// populate them in a vector to print later
			Vector<Movie.Movie> movies = new Vector<Movie.Movie>();
			while (rs.next()) {
				movies.add(new Movie.Movie(rs.getString("id"), rs
						.getString("title"), rs.getString("year"), rs
						.getString("director"), rs.getString("banner_url"), rs
						.getString("trailer_url")));
			}
			
			session.setAttribute("actorMovies", movies);
			
			String url = "/actor.jsp";

			ServletContext sc = getServletContext();
			RequestDispatcher rd = sc.getRequestDispatcher(url);

			rd.forward(request, response);
			
			statement.close();
			dbcon.close();
		} catch (SQLException ex) {
			while (ex != null) {
				System.out.println("SQL Exception:  " + ex.getMessage());
				ex = ex.getNextException();
			} // end while
		} // end catch SQLException

		catch (java.lang.Exception ex) {
			out.println("<HTML>" + "<HEAD><TITLE>" + "MovieDB: Error"
					+ "</TITLE></HEAD>\n<BODY>" + "<P>SQL error in doGet: "
					+ ex.getMessage() + "</P></BODY></HTML>");
			return;
		}

	}
}
