package Actor;

public class Actor 
{
	public String id;
	public String firstName;
	public String lastName;
	public String dob;
	public String photoUrl;
	public String fullName;
	
	public Actor(String id, String first, String last, String dob, String photoUrl)
	{
		this.id = id;
		this.firstName = first;
		this.lastName = last;
		this.dob = dob;
		this.photoUrl = photoUrl;
		this.fullName = first + " " + last;
	}
	public void setAttributes(String id, String first, String last, String dob, String photoUrl)
	{
		this.id = id;
		this.firstName = first;
		this.lastName = last;
		this.dob = dob;
		this.photoUrl = photoUrl;
		this.fullName = first + " " + last;
	}
}
