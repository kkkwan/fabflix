package com.FabFlix5;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


import java.io.*;
import java.util.Calendar;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class Popup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Popup() {
		super();
		// TODO Auto-generated constructor stub
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		String password = (String) session.getAttribute("password");
		List<MovieListing> movies = new ArrayList<MovieListing>();
		try {
			Connection dbcon = Database.SQLPoolBridge.getConnection();
			
			// Declare our statement
			Statement statement = dbcon.createStatement();
			Statement statement3 = dbcon.createStatement();					

			int id = -1;

			if (request.getParameter("id") != null)
				id = Integer.parseInt(request.getParameter("id"));


			String query = "SELECT distinct * FROM movies m, stars s, stars_in_movies sm "
					+ "WHERE m.id = \"" + id + "\""
					+ " AND m.id = sm.movie_id AND s.id = sm.star_id;";

			ResultSet rs = statement.executeQuery(query);
			int year = -1;
			String banner = "";
			String title = "";
			String director = "";
			while(rs.next()){
				title = rs.getString("title");;
				banner = rs.getString("banner_url");
				year = rs.getInt("year");
				director = rs.getString("director");
			}

			String starQuery = "select s.first_name as first_name, s.last_name as last_name "
					+ "from movies m, stars s, stars_in_movies sm "
					+ "where m.id = \""
					+ id
					+ "\" and m.id = sm.movie_id and s.id = sm.star_id";
			ResultSet starResults = statement3.executeQuery(starQuery);

			List<String> starList = new ArrayList<String>();
			while (starResults.next()) {
				starList.add(starResults.getString("first_name") + " "
						+ starResults.getString("last_name"));
			}

			String popup = "<img src=\"" + banner + "\" height=200 width=150 style=\"margin-right:15px; float:left\"/>"
					+"<u><b><i>" + title + " ("+ year +")"+"</i></b></u>"
					+"<br/><br/><b>Banner URL: </b>" + banner + ""
					+"<br/><br/><b>Directed by: </b>" + director + ""
					+"<br/><br/><b>Starring:</b>"
					+" <ul style=\"list-style-type:none; list-style-position: inside; text-indent:30px;\">";

			for (int i = 0; i < starList.size(); i++){
				popup += "<li>" + starList.get(i) + "</li>";
			}
			popup += "</ul>";

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println(popup);

			rs.close();
			starResults.close();
			dbcon.close();
			statement.close();
			statement3.close();

		} catch (SQLException e) {
			PrintWriter out = response.getWriter();
			out.println("Something went wrong on FabFlix! If you see an admin, pass this along: " + e.getMessage());
			System.out.println("Something went wrong: " + e.getMessage());
		} catch (java.lang.Exception e) {
			PrintWriter out = response.getWriter();
			out.println("Something went wrong on FabFlix! If you see an admin, pass this along: " + e.getMessage());
			System.out.println("Something went wrong: " + e.getMessage());
			return;
		}
	}

}
