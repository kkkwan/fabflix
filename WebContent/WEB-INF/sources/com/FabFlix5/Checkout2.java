package com.FabFlix5;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Checkout2
 */
//@WebServlet("/Checkout2")
public class Checkout2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Checkout2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("beggining");
		String url = "";
		HttpSession session = request.getSession();
		
		try {
			Connection dbcon = Database.SQLPoolBridge.getConnection();

			// Declare our statement
			Statement statement = dbcon.createStatement();
			String cardNum = (String) request.getParameter("card_num");
			String expDate = (String) request.getParameter("exp_date");
			String firstName = (String) request.getParameter("first_name");
			String lastName = (String) request.getParameter("last_name");
			String cid = (String) session.getAttribute("cid");
			Cart cart = (Cart) session.getAttribute("cart");

			System.out.println(cardNum);
			System.out.println(expDate);
			System.out.println(firstName);
			System.out.println(lastName);

			String query = "SELECT * from creditcards" + " WHERE id = \""
					+ cardNum + "\"";
			
			System.out.println(query);

			ResultSet resultSet = statement.executeQuery(query);

			int c_ID = 0;
			String u_FN = "", u_LN = "", c_EX = "";

			if (resultSet.next()) {
				c_ID = resultSet.getInt("id");
				u_FN = resultSet.getString("first_name");
				u_LN = resultSet.getString("last_name");
				c_EX = resultSet.getString("expiration");

				if (firstName.equals(u_FN) && lastName.equals(u_LN)
						&& expDate.equals(c_EX)) {
					// information is verified, show some kind of confirmation
					// insert stuff to table
					// order confirmation page
					System.out.println("good credit");
					for (Map.Entry<Item, Integer> entry : cart.cart.entrySet()) {
						System.out.println("entry: " + entry);
						String mid = entry.getKey().movieId;
						String insertQuery = "INSERT INTO sales (id, customer_id,movie_id, sale_date) "
								+ "VALUES (id," + cid+ "," + mid + ", curdate() )";
						statement.executeUpdate(insertQuery);
					}
					cart.emptyCart();
				} else {
					System.out.println("First error");
					// error
				}
			} else {
				System.out.println("Second error");
				// tell user credit card info does not exist
			}
			
			System.out.println("end of flow");

			// Insert SQL at check out
			//
			// INSERT INTO sales (id, customer_id,movie_id, sale_date)
			// VALUES (id,490007, 490008, curdate() );

			// Order Confirmation: page
			//
			// select s.id, s.sale_date, m.title from sales s, movies m,
			// customers c
			// where s.movie_id = m.id and s.customer_id = c.id;

			// session.setAttribute("someName", variable);

			resultSet.close();
			dbcon.close();
			statement.close();

			url = "/confirmation.jsp";

			ServletContext sc = getServletContext();
			RequestDispatcher rd = sc.getRequestDispatcher(url);

			rd.forward(request, response);
		} catch (SQLException e) {
			System.out.println("Something went wrong: " + e.getMessage());
		} catch (java.lang.Exception e) {
			System.out.println("Something went wrong: " + e.getMessage());
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
