package com.FabFlix5;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SearchMovie
 */
//@WebServlet("/SearchMovie")
public class SearchMovie extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchMovie() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url = "";
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		String password = (String) session.getAttribute("password");
		List<MovieListing> movies = new ArrayList<MovieListing>();

		try {
    		Connection dbcon = Database.SQLPoolBridge.getConnection();

			// Declare our statement
			Statement statement = dbcon.createStatement();
			Statement statement2 = dbcon.createStatement();
			Statement statement3 = dbcon.createStatement();
			String movieName = (String) request.getParameter("movieName");
			String genreName = (String) request.getParameter("genre");
			String titleName = (String) request.getParameter("titleName");
			String movieYear = (String) request.getParameter("movieYear");
			String movieDirector = (String) request.getParameter("movieDirector");
			String movieStar = (String) request.getParameter("movieStar");
			
			
			System.out.println(movieName + " " + movieYear + " " + movieDirector + " " + movieStar); 
			
			
			String query = "";
			
			int page = 1;
			int records_per_page = 5;
			String order_by = "title";
			String order_type = "asc";

			if (request.getParameter("page") != null)
				page = Integer.parseInt(request.getParameter("page"));
			if (request.getParameter("amt") != null)
				records_per_page = Integer
						.parseInt(request.getParameter("amt"));
			if (request.getParameter("by") != null)
				order_by = request.getParameter("by");
			if (request.getParameter("order") != null)
				order_type = request.getParameter("order");

			if (movieName == "")
				movieName = null;
			if (titleName == "")
				titleName = null;
			if (genreName == "")
				genreName = null;
			if (movieDirector == "")
				movieDirector = null;
			if (movieStar == "")
				movieStar = null;
			if (movieYear == "")
				movieYear = null;
			
			System.out.println("movieName: " + movieName);
			System.out.println("genreName: " + genreName);
			System.out.println("titleName: " + titleName);
			System.out.println("movieDirector: " + movieDirector);
			System.out.println("movieStar: " + movieStar);
			System.out.println("movieYear: " + movieYear);
			
			
			//regular/adv search case
			if (movieName != null || movieYear != null || movieDirector != null || movieStar != null)
			{
				query = "SELECT distinct m.title as title, m.year as year, m.director as director, m.id as id, m.banner_url as banner_url "
							+ "FROM movies as m, stars as s, stars_in_movies as sim WHERE";
				
				session.setAttribute("movieName", null);
				session.setAttribute("genre", null);
				session.setAttribute("titleName", null);
				session.setAttribute("movieYear", null);
				session.setAttribute("movieDirector", null);
				session.setAttribute("movieStar", null);
				if (movieName != null)
				{
					System.out.println("AA: " + query);
					session.setAttribute("movieName", movieName);
					query += " LOWER(m.title) LIKE \"%" + movieName.toLowerCase() + "%\" AND";
					System.out.println("ZZ: " + query);
				}
				if (movieYear != null)
				{
					System.out.println("movieyear");
					session.setAttribute("movieYear", movieYear);
					query += " m.year=\"" + movieYear + "\" AND";
				}
				if (movieDirector != null)
				{
					System.out.println("moviedirector");

					session.setAttribute("movieDirector", movieDirector);
					query += " LOWER(m.director) LIKE \"%" + movieDirector.toLowerCase() + "%\" AND";
				}
				if (movieStar != null)
				{
					System.out.println("moviestar");

					session.setAttribute("movieStar", movieStar);
					String[] name = movieStar.split(" ");
					int namelen = name.length;
					
					if (namelen == 2)
					{
						query += " m.id=sim.movie_id AND s.id=sim.star_id AND LOWER(s.first_name) LIKE \"%" + name[0].toLowerCase() + "%\" AND LOWER(s.last_name) LIKE \"%" + name[1].toLowerCase() + "%\"";
					}
					else
					{
						query += " m.id=sim.movie_id AND s.id=sim.star_id AND (LOWER(s.first_name) LIKE \"%" + name[0].toLowerCase() + "%\" OR LOWER(s.last_name) LIKE \"%" + name[0].toLowerCase() + "%\")"; 
					}
					
				}
				System.out.println("HEREEE: " + query);

				//trim AND from query
				String lastWord = query.substring(query.lastIndexOf(" ")+1);
				lastWord.trim();
				if (lastWord.equals("AND"))
				{
					query = query.substring(0,query.length() - 3);
					query.trim();
				}
				
				System.out.println(query);
				
			} else if (titleName != null) {
				session.setAttribute("genreName", null);
				session.setAttribute("movieName", null);
				session.setAttribute("movieYear", null);
				session.setAttribute("movieDirector", null);
				session.setAttribute("movieStar", null);
				query = "SELECT * from movies where title like \"" + titleName
						+ "%\"";
			} else if (genreName != null) {
				session.setAttribute("movieName", null);
				session.setAttribute("titleName", null);
				session.setAttribute("movieYear", null);
				session.setAttribute("movieDirector", null);
				session.setAttribute("movieStar", null);
				query = "select distinct m.title as title, m.year as year, m.director as director, m.id as id, m.banner_url as banner_url from "
						+ "movies m, stars s, stars_in_movies sm, genres g, genres_in_movies gm "
						+ "where m.id = sm.movie_id and s.id = sm.star_id and g.id = gm.genre_id and m.id = gm.movie_id "
						+ "and g.name = \"" + genreName + "\"";
			} 
			
			String query2 = "";

			String order_query = " ORDER BY " + order_by; // Ascending

			query2 = query + order_query;
			if (order_type.equals("dsc"))
				query2 += " DESC ";

			String count_query = "SELECT count(*) as c FROM (" + query
					+ ") as result"; // counts the number of results

			query2 += " LIMIT " + records_per_page + " OFFSET "
					+ records_per_page * (page - 1); // what to display


			// Perform the query
			ResultSet count_result = statement.executeQuery(count_query);

			count_result.next();
			int num_records = count_result.getInt("c");

			int num_pages = (int) Math.ceil(num_records * 1.0
					/ records_per_page);

			// incase the user puts a page number that doesn't exist
			if (page > num_pages)
				page = num_pages;
			if (page < 1)
				page = 1;

			session.setAttribute("num_records", num_records);
			session.setAttribute("num_pages", num_pages);
			session.setAttribute("current_page", page);
			session.setAttribute("records_per_page", records_per_page);
			session.setAttribute("order_by", order_by);
			session.setAttribute("order_type", order_type);

			// Performs the query that limits the amount displayed and where to
			// display from
			ResultSet rs = statement.executeQuery(query2);

			session.setAttribute("page_results", rs);

			// Perform the query
			//ResultSet rs = statement.executeQuery(query);
			// Iterate through each row of rs
			while (rs.next()) {
				// Get list of genres
				String genreQuery = "select g.name as genre from movies m, genres g, genres_in_movies gm where m.title = \""
						+ rs.getString("title")
						+ "\" and m.id = gm.movie_id and g.id = gm.genre_id";
				ResultSet genreResults = statement2.executeQuery(genreQuery);
				List<String> genreList = new ArrayList<String>();
				while (genreResults.next()) {
					genreList.add(genreResults.getString("genre"));
				}
				// Get list of stars
				String starQuery = "select s.first_name as first_name, s.last_name as last_name from movies m, stars s, stars_in_movies sm where m.title = \""
						+ rs.getString("title")
						+ "\" and m.id = sm.movie_id and s.id = sm.star_id";
				ResultSet starResults = statement3.executeQuery(starQuery);
				List<String> starList = new ArrayList<String>();
				while (starResults.next()) {
					starList.add(starResults.getString("first_name") + " "
							+ starResults.getString("last_name"));
				}

				MovieListing ml = new MovieListing(rs.getString("title"),
						rs.getString("year"), rs.getString("director"),
						rs.getString("id"), starList, genreList,
						rs.getString("banner_url"), null);
				movies.add(ml);

				genreResults.close();
				starResults.close();
			}
			
			session.setAttribute("movieList", movies);
			session.setAttribute("movieName", movieName);
			session.setAttribute("genreName", genreName);
			session.setAttribute("titleName", titleName);

			count_result.close();
			dbcon.close();
			rs.close();
			statement.close();
			statement2.close();
			statement3.close();

			url = "/search.jsp";

			ServletContext sc = getServletContext();
			RequestDispatcher rd = sc.getRequestDispatcher(url);

			rd.forward(request, response);

			
			
			
		} catch (SQLException e) {
			PrintWriter out = response.getWriter();
			out.println("Something went wrong on FabFlix! If you see an admin, pass this along: " + e.getMessage());
			System.out.println("Something went wrong: " + e.getMessage());
		} catch (java.lang.Exception e) {
			PrintWriter out = response.getWriter();
			out.println("Something went wrong on FabFlix! If you see an admin, pass this along: " + e.getMessage());
			System.out.println("Something went wrong: " + e.getMessage());
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
