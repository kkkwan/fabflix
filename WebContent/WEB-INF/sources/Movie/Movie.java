package Movie;

public class Movie 
{
	public String id;
	public String title;
	public String year;
	public String director;
	public String bannerUrl;
	public String trailerUrl;
	
	public Movie(String id, String title, String year, String director, String bannerUrl, String trailerUrl)
	{
		this.id = id;
		this.title = title;
		this.year = year;
		this.director = director;
		this.bannerUrl = bannerUrl;
		this.trailerUrl = trailerUrl;
	}
	
	public void setAttributes(String id, String title, String year, String director, String bannerUrl, String trailerUrl)
	{
		this.id = id;
		this.title = title;
		this.year = year;
		this.director = director;
		this.bannerUrl = bannerUrl;
		this.trailerUrl = trailerUrl;
	}
}
