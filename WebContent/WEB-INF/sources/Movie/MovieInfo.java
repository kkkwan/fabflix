package Movie;


import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/*
 * 
 * This class is in desperate need of refactoring
 * 
 */

public class MovieInfo extends HttpServlet
{
	/*
	 * CHANGE ME TO WHAT YOUR PROJECT/WAR FILE IS CALLED
	 */
	private static final String serverRoot = "/FabFlix5";
	
//	private final ServletContext ctx = getServletContext();
	 
	
	
	
	public String getServletInfo()
    {
       return "Displays information related to a single movie and involved actors from the database";
    }

    // Use http GET

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
    	
//    	System.out.println(contextPath);

        response.setContentType("text/html");    // Response mime type
        String movieId = request.getParameter("id");
        
        PrintWriter out = response.getWriter();
        if (movieId == null)
        {
        	//go to some other page
        	out.println("MovieInfo:doGet::EMPTY_QUERY");
        	return;
        }
        
        
        try
        {
        	//user SQLPoolBridge and get a Pooled Connection
           Connection dbcon = Database.SQLPoolBridge.getConnection();
           // Declare our statement
           Statement statement = dbcon.createStatement();
           
		   HttpSession session = request.getSession();

           String genreQuery = "select g.name as genre from movies m, genres g, genres_in_movies gm where m.id = \""
					+ movieId
					+ "\" and m.id = gm.movie_id and g.id = gm.genre_id";
		   ResultSet genreResults = statement.executeQuery(genreQuery);
		   List<String> genreList = new ArrayList<String>();
			while (genreResults.next()) {
				genreList.add(genreResults.getString("genre"));
			}
		   session.setAttribute("genreList", genreList);
			
			
           //initial movie query
           ResultSet rs = statement.executeQuery("SELECT * FROM movies WHERE id=\"" + movieId + "\"");
           
           
           //default to a "move not found: error page if we get a bad query
           if (!rs.next())
           {
        	   out.println("MovieInfo:doGet::MOVIE_NOT_FOUND");
        	   return;
           }
           
           //grab results from query
           Movie mov = new Movie(movieId,rs.getString("title"),rs.getString("year"),rs.getString("director"),rs.getString("banner_url"),rs.getString("trailer_url"));
           session.setAttribute("movie", mov);
           
           //get related actors from query
           rs = statement.executeQuery("SELECT s.id, s.first_name, s.last_name, s.dob, s.photo_url "
           								+ "FROM stars as s, stars_in_movies as s2m "
           								+ "WHERE s.id = s2m.star_id AND s2m.movie_id =\"" + movieId + "\"");
           
           //populate them in a vector to print later
           Vector<Actor.Actor> actors = new Vector<Actor.Actor>();
           while(rs.next())
           {
        	   actors.add(new Actor.Actor(rs.getString("id"),rs.getString("first_name"),rs.getString("last_name"),rs.getString("dob"),rs.getString("photo_url")));
           }
           session.setAttribute("actors", actors);
           
			String url = "/movie.jsp";

			ServletContext sc = getServletContext();
			RequestDispatcher rd = sc.getRequestDispatcher(url);

			rd.forward(request, response);
           //done with queries
           statement.close();
           dbcon.close();

        }
        catch (SQLException ex) 
        {
            while (ex != null) 
            {
                  System.out.println ("SQL Exception:  " + ex.getMessage ());
                  ex = ex.getNextException ();
            }  // end while
        }  // end catch SQLException

        catch(java.lang.Exception ex)
        {
        	out.println("<HTML>" +
                          "<HEAD><TITLE>" +
                          "MovieDB: Error" +
                          "</TITLE></HEAD>\n<BODY>" +
                          "<P>SQL error in doGet: " +
                          ex.getMessage() + "</P></BODY></HTML>");
            return;
        } //end catch java.lang.Exception
           
    } //  doGet()
} //class  MovieInfo
