package Database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class SQLPoolBridge {
	
	private static DataSource dataSource;

	static
	{
		try
		{
			//voodoo magic ahead
			final Context initCtx = new InitialContext();
			
			final Context envCtx = (Context)initCtx.lookup("java:comp/env");
			
			//this datasource is referenced in /META-INF/context.xml
			//as well as in web.xml
			dataSource = (DataSource) envCtx.lookup("jdbc/moviedb");
		}
		catch (NamingException e)
		{
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static Connection getConnection()
	{
		try
		{
			return dataSource.getConnection();
		}
		catch (SQLException e)
		{
			throw new ExceptionInInitializerError(e);
		}
	}
	
	
} //SQLPoolBridge
