<%@ page import="com.FabFlix5.MovieListing, java.util.*" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<% if (session.getAttribute("loginSuccess") == null || !((Boolean)session.getAttribute("loginSuccess"))) { %>
		<% System.out.println("Not logged in"); //This is not working right now %>
		<jsp:forward page="/index.jsp" />
	<% } %>
	
	<% List<MovieListing> movies = (List<MovieListing>) session.getAttribute("movieList"); %>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>FabFlix</title>
	<style>
		body { padding-top: 70px; }
		.a {word-wrap: break-word;}
		.tooltip-inner {
		    white-space: pre-wrap;
		    min-width: 100px;
		}
		.mytooltip + .tooltip > .tooltip-inner {
				  background-color: #e7e7e7; 
				  border: 1px solid #ccc; 
				  color:#000; 
				  height:250px;
				  max-width:none;
				  width:500px; 
				  text-align:left;
		     	  vertical-align: text-top;
		}
		.mytooltip + .tooltip.right .tooltip-arrow {
				
				  border-right-color: #ccc; /* black */
				
		}
	</style>
	
	
	<html>
<body>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'right'
    });
});


</script>
<script language="javascript" type="text/javascript">

//Browser Support Code
function ajaxFunction(){
	var ajaxRequest;  // The variable that makes Ajax possible!

	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server
	
	
	$('.mytooltip').mouseover(function(){
		  titleLink = $(this).attr('href');
	//var titleLink = document.getAttribute('href');
	var movieID = titleLink.split("=")[1]; 
	var attID = "#" + movieID;
	console.log(attID);
	
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			  //var newTitle = ajaxRequest.responseText;
			  //document.getElementById("movTitle").setAttribute('data-original-title', newTitle);
			 
			  //document.getElementById("movTitle").setAttribute('data-original-title', movieID);
			  
			  var newTitle = ajaxRequest.responseText;
			  //var test = $(attID).attr('data-original-title');
			  //console.log(test);
			  $(attID).attr('data-original-title', newTitle);
		}
	}
	ajaxRequest.open("GET", "${pageContext.request.contextPath}/servlet/popup?id="+movieID, true);
	ajaxRequest.send(null);
	
	
	var titleLink;
	
		  console.log(titleLink);

	});
	 // console.log(titleLink);
}

</script>
	
	
	
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FabFlix</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/home.jsp">HOME</a></li>
        <li><a href="${pageContext.request.contextPath}/advancedsearch.jsp">ADVANCED SEARCH</a></li>
        <li><a href="${pageContext.request.contextPath}/cart.jsp">CART</a></li>
        <li><a href="${pageContext.request.contextPath}/servlet/checkout">CHECKOUT</a></li>        
        </ul>
        <h5><div align="right">Welcome ${username}! <a href="${pageContext.request.contextPath}/servlet/logout"><label for="ex1">Logout</label></a></div></h5>
        </div>
        </div>
        </nav>

    <div class="container">
    	<h2>Search results</h2>
      	<div class="table-responsive">          
			<table class="table">
        		<thead>
	          		<tr>
	          			<th></th>
	           			<th>Title</th>
	            		<th>Year</th>
	            		<th>Director</th>
	            		<th>Movie ID</th>
	            		<th>Stars</th>
	            		<th>Genre</th>
	            		<th>Price</th>
	          		</tr>
        		</thead>
				<tbody>
					<% for (MovieListing ml : movies) { %>
	          			<tr>
	            			<td>
	            			<% if (ml.bannerUrl != null && ml.bannerUrl != "") { %>
	            				<div style="padding-left:20px; padding-right:20px;"><img src="<%= ml.bannerUrl %>" height=100 width=65 /></div>
	            			<% }%>
	            			<br>
	            			<div style="padding-left:8px"><a href="${pageContext.request.contextPath}/servlet/cart?action=add&mid=<%=ml.movieId %>"><button type="submit">Add to cart</button></a></div>
	            			</td>
	            			<td><a href="${pageContext.request.contextPath}/movie?id=<%=ml.movieId%>" data-toggle="tooltip" data-original-title="Loading..." data-html="true" id="<%=ml.movieId%>" class="mytooltip" onmouseover="ajaxFunction();"><%= ml.title %></a></div>
	            			</td>
	            			<td><%= ml.year %></td>
	            			<td><%= ml.director %></td>
	            			<td><%= ml.movieId %></td>
	            			<td><% 
	            				for (String star : ml.stars) { %>
	            					<a href="${pageContext.request.contextPath}/actor?name=<%=star%>"><%= star %></a>
	            					<% if(!star.equals(ml.stars.get(ml.stars.size()-1))) { %>
	            					,
	            					<% } %> 
	            				<% } %>
	            			</td>
	            			<td><% 
	            				for (String genre : ml.genres) { %>
	            					<%= genre %>
	            					<% if(!genre.equals(ml.genres.get(ml.genres.size()-1))) { %>
	            					,
	            					<% } %> 
	            				<% } %>
	            			</td>
	            			<td><%= ml.price %></td>
	          			</tr>
          			<% } %>
        		</tbody>
        	</table>
        	<div>
	  <ul class="pager">
	  	<%if((Integer)session.getAttribute("current_page") > 1){ %>
	    	<li><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&page=${current_page - 1}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">Prev</a></li>
	    <%} else {%>
	    	<li class="disabled"><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&page=${current_page}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">Prev</a></li><%} %>
	    
	    <%if((Integer)session.getAttribute("current_page") < (Integer)session.getAttribute("num_pages")){ %>
	    	<li><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&page=${current_page + 1}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">Next</a></li>
	    <%} else {%>
	    	<li class="disabled"><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&page=${current_page}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">Next</a></li><%} %>
	  </ul>
</div>
<div class="text-center">

	<ul class="pagination">
	<li><a>Page Number</a></li>
	<% for (int i = 1; i <= (Integer)session.getAttribute("num_pages"); i++) {
		if ((Integer)session.getAttribute("current_page") == i){ %>
			<li class="active"><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&genre=${genreName}&movieName=${movieName}&page=<%=i%>&amt=${records_per_page}"><%= i %></a></li>
	<%}	else {%>
	  <li><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&genre=${genreName}&movieName=${movieName}&page=<%=i%>&amt=${records_per_page}"><%= i %></a></li>
	<%}
	}%>
	</ul>
</div>
<div class="text-center">
	<ul class="pagination">
	<li><a>Results Per Page</a></li>
	<% for (int i = 5; i <= 20; i+=5) {
		if ((Integer)session.getAttribute("records_per_page") == i){ %>
			<li class="active"><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&page=${current_page}&genre=${genreName}&movieName=${movieName}&amt=<%= i %>"><%= i %></a></li>
	<%}	else {%>
	  <li><a href="${pageContext.request.contextPath}/servlet/search?by=${order_by}&titleName=${titleName}&order=${order_type}&page=${current_page}&genre=${genreName}&movieName=${movieName}&amt=<%=i%>"><%= i %></a></li>
	<%}
	}%>
	</ul>
</div>

<div class="text-center">

	<a href="${pageContext.request.contextPath}/servlet/search?by=title&order=asc&page=${current_page}&titleName=${titleName}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">
	<button type="button" class="btn btn-default btn-sm">
	  	<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span> Title
	</button>
	</a>
	
	<a href="${pageContext.request.contextPath}/servlet/search?by=title&order=dsc&page=${current_page}&titleName=${titleName}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">
	<button type="button" class="btn btn-default btn-sm">
	  	<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span> Title
	</button>
	</a>
	
	<a href="${pageContext.request.contextPath}/servlet/search?by=year&order=asc&page=${current_page}&titleName=${titleName}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">
	<button type="button" class="btn btn-default btn-sm">
	  	<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span> Year
	</button>
	</a>
	
	<a href="${pageContext.request.contextPath}/servlet/search?by=year&order=dsc&page=${current_page}&titleName=${titleName}&genre=${genreName}&movieName=${movieName}&amt=${records_per_page}">
	<button type="button" class="btn btn-default btn-sm">
	  	<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span> Year
	</button>
	</a>
</div>
        	
      	</div>
	</div>
</body>
</html>