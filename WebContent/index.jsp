<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<% if (session.getAttribute("loginSuccess") != null && (Boolean)session.getAttribute("loginSuccess")) { %>
		<% System.out.println("Already logged in"); //This is not working right now %>
		<jsp:forward page="/home.jsp" />
	<% } %>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>FabFlix</title>
</head>
<body>
	<center><h1>FabFlix Login</h1></center>      
	<br>
	<div class="container">
  		<form role="form" action="${pageContext.request.contextPath}/servlet/login" method="post">
  	  		<div class="form-group">
				<label for="usr">Username:</label>
   	    		<input type="text" class="form-control" name="username">
   	  		</div>
      		<div class="form-group">
				<label for="pwd">Password:</label>
      			<input type="password" class="form-control" name="password">
			</div>
			<br>
			<center>
				<div class="form-group">
        			<button type="submit" class="btn btn-default">Submit</button>
      			</div>
      			<% if (session.getAttribute("loginSuccess") != null && !(Boolean)session.getAttribute("loginSuccess")) { %>
      				<font color="red" size=+2>Incorrect login, please try again</font>
      				<% System.out.println("Login incorrect"); %>
      			<% } %>
			</center>
		</form>
	</div>

</body>
</html>