<%@ page import="java.util.*" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<% if (session.getAttribute("loginSuccess") == null || !((Boolean)session.getAttribute("loginSuccess"))) { %>
		<% System.out.println("Not logged in"); //This is not working right now %>
		<jsp:forward page="/index.jsp" />
	<% } %>
	
	<% List<String> genres = (List<String>) session.getAttribute("genreList"); %>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>FabFlix</title>
	
	<script type="text/javascript">
		window.onload = function() {
        	var a = document.getElementById("genreId");

        	a.onclick = function() {
				console.log("clicked");
        		return false;
        	}
        }
	</script>
	<style>
		body { padding-top: 70px; }
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FabFlix</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/home.jsp">HOME</a></li>
        <li class="active"><a href="${pageContext.request.contextPath}/advancedsearch.jsp">ADVANCED SEARCH<span class="sr-only">(current)</span></a></li>
        <li><a href="${pageContext.request.contextPath}/cart.jsp">CART</a></li>
        <li><a href="${pageContext.request.contextPath}/servlet/checkout">CHECKOUT</a></li>
        </ul>
        <h5><div align="right">Welcome ${username}! <a href="${pageContext.request.contextPath}/servlet/logout"><label for="ex1">Logout</label></a></div></h5>
        </div>
        </div>
        </nav>

<form action="${pageContext.request.contextPath}/servlet/search" method="post">
<div class="container">
		  	
			<div class="form-group">
    			<div class="col-md-4">
        			<label for="ex1">Search for a movie by name</label>
        			<input class="form-control" name="movieName" type="text">
      			</div>
			</div>
	</div>
	
	<div class="container">
			<div class="form-group">
    			<div class="col-md-4">
        			<label for="ex1">Search for a movie by year</label>
        			<input class="form-control" name="movieYear" type="text">
      			</div>
			</div>
	</div>
	
	<div class="container">
			<div class="form-group">
    			<div class="col-md-4">
        			<label for="ex1">Search for a movie by director</label>
        			<input class="form-control" name="movieDirector" type="text">
      			</div>
			</div>
	</div>
	
	<div class="container">
			<div class="form-group">
    			<div class="col-md-4">
        			<label for="ex1">Search for a movie by star</label>
        			<input class="form-control" name="movieStar" type="text">
      			</div>
			</div>
		
	</div>
	<div class="container">
			<div class="form-group">
    			<div class="col-md-4">
        			<button type="submit" value="Search">Submit</button>
      			</div>
			</div>
		
	</div>
	
</form>
</body>
</html>