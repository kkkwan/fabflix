package com.FabFlix5;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

public class AjaxTest extends HttpServlet {

  public void doGet(HttpServletRequest request, HttpServletResponse response)
                               throws ServletException, IOException {
    response.setContentType("text/html");
	String inboxText = request.getParameter("movieName");
	String[] sInboxText = inboxText.split(" ");
	
	int pos = 0;
	String query = "SELECT title from movies where ";
	String query2 = "";
	
	try {
		Connection dbcon = Database.SQLPoolBridge.getConnection();
		Statement statement = dbcon.createStatement();
	
		for (String s : sInboxText)
		{
			if (pos == 0)
			{
				query += "title like \"%" + s + "%\"";
			}
			else if (pos == sInboxText.length - 1)
			{
				String lastWord = sInboxText[sInboxText.length - 1];
				query2 = query;
				query2 += "and title like \"%" + s + "%\"";
			} else
			{
				query += "and title like \"%" + s + "%\"";
			}
			pos++;
		}
		
		List<String> selectList = new ArrayList<String>();

		if (sInboxText.length > 1)
		{
			System.out.println("_______");
			System.out.println("query2: " + query2);
			ResultSet resultSet2 = statement.executeQuery(query2);
			while (resultSet2.next())
			{
				System.out.println(resultSet2.getString("title"));
				selectList.add(resultSet2.getString("title"));
			}
			
			resultSet2.close();
		} else
		{
			System.out.println("query: " + query);
			ResultSet resultSet = statement.executeQuery(query);
		
			while (resultSet.next())
			{
				System.out.println(resultSet.getString("title"));
				selectList.add(resultSet.getString("title"));
			}
			
			resultSet.close();
		}
			
	    PrintWriter out = response.getWriter();
	    out.print(
	    "<select onchange=\"selectedItem(this)\" id=\"listBox\" name=\"listBox\" size=\"5\" width=\"350\" style=\"width: 350px\">");
	    for (String str : selectList)
	    {
		    out.print("<option>" + str + "</option>");
	    }
	    out.println("</select>");
	    
		dbcon.close();
		statement.close();
	} catch (SQLException e) {
		System.out.println("Something went wrong: " + e.getMessage());
	} catch (java.lang.Exception e) {
		System.out.println("Something went wrong: " + e.getMessage());
		return;
	}
  }
}
