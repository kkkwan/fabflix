package com.FabFlix5;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class Cart
{
	public HashMap<Item, Integer> cart;
	
	public Cart()
	{
		cart = new HashMap<Item, Integer>();
	}
	
	public void addItem(String itemId, String movieTitle)
	{
		for (Map.Entry<Item, Integer> entry : cart.entrySet())
		{
			if (entry.getKey().movieId.equals(itemId))
			{
				cart.put(entry.getKey(), (entry.getValue() + 1));
				return;
			}
		}
		cart.put(new Item(itemId, movieTitle), 1);
	}
	
	public void removeItem(String itemId)
	{
		for (Map.Entry<Item, Integer> entry : cart.entrySet())
		{
			if (entry.getKey().movieId.equals(itemId))
			{
				System.out.println("Removing...");
				cart.remove(entry.getKey());
				System.out.println("Removed");
			}
		}
	}
	
	public void setQuantity(String itemId, int quantity)
	{
		for (Map.Entry<Item, Integer> entry : cart.entrySet())
		{
			if (entry.getKey().movieId.equals(itemId))
			{
				if (quantity == 0)
					cart.remove(entry.getKey());
				else
					cart.put(entry.getKey(), quantity);
			}
		}
	}
	
	public void emptyCart()
	{
		cart.clear();
	}
}