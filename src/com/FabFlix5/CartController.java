package com.FabFlix5;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CartController
 */
//@WebServlet("/CartController")
public class CartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public Cart shoppingCart;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartController() {
        super();
        // TODO Auto-generated constructor stub
        shoppingCart = new Cart();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = null;
		int movieId;
		HttpSession session = request.getSession();

		if (session.getAttribute("cart") == null)
		{
			System.out.println("Cart is empty, creating a new one");
			session.setAttribute("cart", shoppingCart);
		} else 
		{
			System.out.println("Retrieving already created cart");
			this.shoppingCart = (Cart) session.getAttribute("cart");
			System.out.println("Cart size is :  " + shoppingCart.cart.size());
		}
		
		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
			movieId = Integer.parseInt(request.getParameter("mid"));
		} else 
			return;
		
		try {
    		Connection dbcon = Database.SQLPoolBridge.getConnection();
			Statement statement = dbcon.createStatement();
			String query = "";
			
			if (action.equals("add"))
			{
				System.out.println("Adding to cart: " + Integer.toString(movieId));
				query = "select distinct title from movies where id = " + Integer.toString(movieId);
				ResultSet result = statement.executeQuery(query);
				result.next();
				shoppingCart.addItem(Integer.toString(movieId), result.getString("title"));
				result.close();
				statement.close();
			} else if (action.equals("remove"))
			{
				System.out.println("Removing from cart: " + Integer.toString(movieId));
				shoppingCart.removeItem(Integer.toString(movieId));
			} else if (action.equals("update"))
			{
				String quantity = (String) request.getParameter("quantity");
				System.out.println("Updating cart: " + Integer.toString(movieId) + " | " + quantity);
				shoppingCart.setQuantity(Integer.toString(movieId), Integer.parseInt(quantity));
			} else if (action.equals("clear"))
			{
				System.out.println("Clearing cart");
				shoppingCart.emptyCart();
			}
			
		} catch (SQLException e) {
			System.out.println("Something went wrong: " + e.getMessage());
		} catch (java.lang.Exception e) {
			System.out.println("Something went wrong: " + e.getMessage());
		}
		
		String url = "/cart.jsp";

		ServletContext sc = getServletContext();
		RequestDispatcher rd = sc.getRequestDispatcher(url);

		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
